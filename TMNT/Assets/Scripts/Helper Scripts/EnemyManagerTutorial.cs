﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManagerTutorial : MonoBehaviour
{
    // an Instance that will allow other scripts to use it when needed-
    public static EnemyManagerTutorial instance;
    // You will put here the enemy prefab that will be spawned-
    [SerializeField]
    private GameObject enemyPrefab;
    void Awake()
    {
        //?
        if (instance == null)
            instance = this;
    }

    private void Start()
    {
        // game begins and an enemy is spawned-
        SpawnEnemy();
    }
    // A function that spawns enemys-
    public void SpawnEnemy()
    {
        // spawn enemy prefab on the marked location -
        Instantiate(enemyPrefab, transform.position, Quaternion.identity);
    }
}
