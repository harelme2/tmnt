﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimationDelegate : MonoBehaviour
{
    // Access to the game objects of the colliders-
    public GameObject left_Arm_Attack_Point, right_Arm_Attack_Point, left_Leg_Attack_Point, right_Leg_Attack_Point, stick_Attack_Point;
    // var with time until the enemy stands up-
    public float stand_Up_Timer = 2f;
    // access to player's script-
    private CharacterAnimation animationScript;

    // access to AudioSource with var-
    private AudioSource audioSource;
    // creating audio types as serlized fields-
    [SerializeField]
    private AudioClip whoosh_Sound, fall_Sound, ground_Hit_Sound, dead_Sound;
    //access to enemy movement script with var-
    private EnemyMovement enemy_Movement;
    // acess to the shake camera script using var-
    private ShakeCamera shakeCamera;
    //private CamShake shakeCamera;

    private PlayerMovementTutorial player_Movement;

    private PlayerAttack player_Attack;




    private void Awake()
    {
        // access to the script-
        animationScript = GetComponent<CharacterAnimation>();
        // access to the Audio source-
        audioSource = GetComponent<AudioSource>();
        //check that the enemy is holding the script and then access it-
        if (gameObject.CompareTag(Tags.ENEMY_TAG))
        {
            enemy_Movement = GetComponentInParent<EnemyMovement>();
        }

        if (gameObject.CompareTag(Tags.PLAYER_TAG))
        {
            player_Movement = GetComponentInParent<PlayerMovementTutorial>();
            player_Attack = GetComponentInParent<PlayerAttack>();
        }
        // access to the shake camera script and using the tag-
        shakeCamera = GameObject.FindWithTag(Tags.MAIN_CAMERA_TAG).GetComponent<ShakeCamera>();
        //shakeCamera = GameObject.FindWithTag(Tags.MAIN_CAMERA_TAG).GetComponent<CamShake>();

    }

    void Left_Arm_Attack_On ()
    {
        left_Arm_Attack_Point.SetActive(true);
    }
    void Left_Arm_Attack_Off()
    {
        if (left_Arm_Attack_Point.activeInHierarchy)
        {
            left_Arm_Attack_Point.SetActive(false);

        }
    }
    void Right_Arm_Attack_On()
    {
        right_Arm_Attack_Point.SetActive(true);
    }
    void Right_Arm_Attack_Off()
    {
        if (right_Arm_Attack_Point.activeInHierarchy)
        {
            right_Arm_Attack_Point.SetActive(false);

        }
    }
    void Left_Leg_Attack_On()
    {
        left_Leg_Attack_Point.SetActive(true);
    }
    void Left_Leg_Attack_Off()
    {
        if (left_Leg_Attack_Point.activeInHierarchy)
        {
            left_Leg_Attack_Point.SetActive(false);

        }
    }
    void Right_Leg_Attack_On()
    {
        right_Leg_Attack_Point.SetActive(true);
    }
    void Right_Leg_Attack_Off()
    {
        if (right_Leg_Attack_Point.activeInHierarchy)
        {
            right_Leg_Attack_Point.SetActive(false);

        }
    }

    void TagLeft_Arm()
    {
        left_Arm_Attack_Point.tag = Tags.LEFT_ARM_TAG;
    }

    void UnTagLeft_Arm()
    {
        left_Arm_Attack_Point.tag = Tags.UNTAGGED_TAG;
    }
    
    void TagRight_Arm()
    {
        right_Arm_Attack_Point.tag = Tags.RIGHT_ARM_TAG;
    }

    void UnTagRight_Arm()
    {
        right_Arm_Attack_Point.tag = Tags.UNTAGGED_TAG;
    }

    void TagLeft_Leg()
    {
        left_Leg_Attack_Point.tag = Tags.LEFT_LEG_TAG;
    }

    void UnTagLeft_Leg()
    {
        left_Leg_Attack_Point.tag = Tags.UNTAGGED_TAG;
    }
    
    void TagRight_Leg()
    {
        right_Leg_Attack_Point.tag = Tags.RIGHT_LEG_TAG;
    }

    void UnTagRight_Leg()
    {
        right_Leg_Attack_Point.tag = Tags.UNTAGGED_TAG;
    }

    void TagStick()
    {
        stick_Attack_Point.tag = Tags.STICK_TAG;
    }

    void UnTagStick()
    {
        stick_Attack_Point.tag = Tags.UNTAGGED_TAG;
    }

    void Enemy_StandUp()
    {
        StartCoroutine(StandUpAfterTime());
    }

    IEnumerator StandUpAfterTime()
    {
        // after 2 seconds the enemy will stand up after he's knocked down-
        yield return new WaitForSeconds(stand_Up_Timer);
        animationScript.StandUp();

    }

    // function for sound affects to invoke when attacking-
    public void Attack_FX_Sound()
    {
        // set the volume, type of the audio source-
        audioSource.volume = 0.2f;
        audioSource.clip = whoosh_Sound;
        audioSource.Play();
    }
    public void CharacterDiedSound()
    {
        // set the volume, type of the audio source-
        audioSource.volume = 1f;
        audioSource.clip = dead_Sound;
        audioSource.Play();
    }
    public void Enemy_KnockedDown()
    {
        // set the type of the audio source-
        audioSource.clip = fall_Sound;
        audioSource.Play();
    }
    public void Enemy_HitGround()
    {
        // set the type of the audio source-
        audioSource.clip = ground_Hit_Sound;
        audioSource.Play();
    }
    // function to disable movment when the enemy is knockedout-
    void DisableMovement()
    {
        //turn off the enemy movement script-
        enemy_Movement.enabled = false;
        //set the enemy parent to default layer, the layer isnt "enemy" anymore so no collison-
        transform.parent.gameObject.layer = 0;
    }
    // function to enable on movment when the enemy is knockedout-
    void EnableMovement()
    {
        //turn on the enemy movement script-
        enemy_Movement.enabled = true;
        //turn back the layer to "enemy"-
        transform.parent.gameObject.layer = 10;
    }

    void DisablePlayerMovement()
    {
        player_Movement.DisablePlayerMovement();
    }

    void EnablePlayerMovement()
    {
        player_Movement.EnablePlayerMovement();
    }

    void LockPlayerAnimation()
    {
        player_Attack.LockAnimation();
    }

    void UnLockPlayerAnimation()
    {
        player_Attack.UnLockAnimation();
    }

    // function to activate shake camera -
    void ShakeCameraOnFall()
    {
        // turn the bool from the shake camera script to true-
        shakeCamera.ShouldShake = true;
    }

    // function for death -
    void CharacterDied()
    {
        // invoke the function after 2 seconds-
        Invoke("DeactivateGameObject", 2f);
    }

    // function for deactivate the whole game object of the enemy and spawn a new one-
    void DeactivateGameObject()
    {
        // new enemy will spawn after the death as an instance that is used from the enemy Manager script-
        EnemyManagerTutorial.instance.SpawnEnemy();
        // Enemy will Vanish after death-
        gameObject.SetActive(false);
    }
    
}
