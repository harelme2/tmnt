﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthScript : MonoBehaviour
{
    // Health parmeter for both player and enemy-
    public float health = 100f;
    //Access to player & Enemy scripts-
    private CharacterAnimation animationScript;
    private EnemyMovement enemyMovement;

    // two Bool to check if player/enemy is dead and who is holding the script-
    private bool CharacterDied;
    public bool is_Player;
    // accessing the health UI bar with a var-
    private HealthUI health_UI;
    void Awake()
    {
        // access to the Player's script-
        animationScript = GetComponentInChildren<CharacterAnimation>();
        // checking if it's the player and accessing the health UI bar with a var-
        if (is_Player)
        {
            health_UI = GetComponent<HealthUI>();
        }
    }
    // a function to apply dmage-
    public void ApplyDamage(float damage, bool knockDown)
    {
        // if the character died then we don't need the damage function-
        if (CharacterDied)
            return;
        health -= damage;
        //check if it's the player and display health UI-
        if(is_Player)
        {
            health_UI.DisplayHealth(health);
        }
        // check if health is equal/bellow zero-
        if (health <= 0f)
        {
            // if so, then the character is dead-
            animationScript.Death();
            CharacterDied = true;

            //if is player then deactivate enemy script-
            if (is_Player)
            {
                // acess to Enemy Momvemet script and shutting it down so he will stop attaking him after the player dies-
                GameObject.FindWithTag(Tags.ENEMY_TAG).GetComponent<EnemyMovement>().enabled = false;
            }
            return;
        }
        // is this enemy?-
        if(!is_Player)
        {
            if (knockDown)
            {
                // 0 or 2 will declare wether the enemy will be knockdown or not, 50%-
                if (Random.Range(0, 2) > 0)
                {
                    animationScript.KnockDown();
                }
            }
            else
            {
                // randomize when to start hit animation as we don't want it to happen every punch/kick-
                if(Random.Range(0, 3) > 1)
                {
                    animationScript.Hit();
                }
            }
        }
    }

   
}
