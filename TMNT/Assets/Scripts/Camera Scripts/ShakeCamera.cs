﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakeCamera : MonoBehaviour
{
    // set the power and duration that we are going to use to shake the camera using var-
    public float power = 0.2f;
    public float duration = 0.2f;

    public float slowDownAmount = 1f;
    // creating bool for wether the camera should shake or not-
    private bool should_Shake;
    // var for saving the initial duration-
    private float initialDuration;
    // creating var for a vector 3 transform-
    private Vector3 startPosition;
    void Start()
    {
        // the local position will be local to itself- 
        startPosition = transform.localPosition;
        // setting up the initial duration to be 0.2f -
        initialDuration = duration;
    }

    // Update is called once per frame
    void Update()
    {
        Shake();
    }
    // a function to generate the shake-
    void Shake()
    {
        // check if this is the right condition for the camera to shake-
        if (should_Shake)
        {
            // check if we still have time to shake-
            if (duration > 0f)
            {
                // the new transform of the camera will be random movment * power, basiccly its going to make the camer shake -
                transform.localPosition = startPosition + Random.insideUnitSphere * power;
                // the duration of the shakeness will be the duration - 1f as seconds-
                duration -= Time.deltaTime * slowDownAmount;
            }
            // duration <=0 -
            else
            {
                // camera won't shake & we will reset the duration and position-
                should_Shake = false;
                duration = initialDuration;
                transform.localPosition = startPosition;
            }
        }
    }

    // all this shit is just to give access to the bool, for some reason the first letter is capital-
    public bool ShouldShake
    { 
        get
        {
            return should_Shake;
        }
        set
        {
            should_Shake = value;
        }
    }


}
