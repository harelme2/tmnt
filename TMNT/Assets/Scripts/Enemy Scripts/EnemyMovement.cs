﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    // access to Character Animation script with a var-
    private CharacterAnimation enemyAnim;
    private Rigidbody myBody;
    public float speed = 5f;
    private Transform playerTarget;

    public float attack_Distance = 1f;
    public float chase_Player_After_Attack = 1f;

    private float current_Attack_Time;
    private float default_Attack_Time = 0.7f;

    // two bools declarations in the same line but to of them are not connected-
    private bool followPlayer, attackPlayer;

    private void Awake()
    {
        // Access to Charecter Animation script with var because the script is on the baby enemy-
        enemyAnim = GetComponentInChildren<CharacterAnimation>();

        // access to RigidBoy of the Enemy, no Children because the RigidBody is attached to the father-
        myBody = GetComponent<Rigidbody>();

        // Access to Player's Tag so the Enemy will understand who to follow and attack, using var-
        playerTarget = GameObject.FindWithTag(Tags.PLAYER_TAG).transform;
    }

    void Start()
    {
        followPlayer = true;
        current_Attack_Time = default_Attack_Time;
    }

    // Update & fiXedUpdate will check if either Attack or FollowTarget is necessery-
    void Update()
    {
        Attack();
    }
    void FixedUpdate()
    {
        // we are using Rigidbody so Its in the fixedUpdate-
        FollowTarget();
    }

    void FollowTarget()
    {
        //if we are not allowed to follow the player then we will return-
        if (!followPlayer)
            return;
         //Check if the Distance between enemy and player is bigger then Attack Distance needed-
        if (Vector3.Distance(transform.position, playerTarget.position) > attack_Distance)
        {
            // then we will follow the player and not attack it-
            // first Step, Look at the player via Rotate but with forward direction which is blue arrow (X)-
            transform.LookAt(playerTarget);
            // Second step, Velocity of Enemy RigidBody will go Forward with blue arrow * Speed-
            myBody.velocity = transform.forward * speed;
            // Third Step, Check If Enemy is moving or not to see if Walk Animation is needed, if sqrmagnitud isn't 0 then it moves-
            if (myBody.velocity.sqrMagnitude != 0)
           {
                // then operate walking animation-
                enemyAnim.Walk(true);
            }
        }
        // if the Distance between enemy and player is equal or less then needed for attack distance then attack-
        else if (Vector3.Distance(transform.position, playerTarget.position) <= attack_Distance)
        {
            //Stop Walking (Both velocity and animation) Because we can now attack-
            myBody.velocity = Vector3.zero;
            enemyAnim.Walk(false);
            followPlayer = false;
            attackPlayer = true;
        }
    } 
    void Attack()
    {
        // if we are not supposed to attack player then we will return-
        if (!attackPlayer)
            return;

        // attack starts and is mesured with Time var mesured with real time using timeDeltaTime (time between each frame)-
        current_Attack_Time += Time.deltaTime;
        // if it's bigger then the default attack time then the enemy will attack-
        if (current_Attack_Time > default_Attack_Time)
        {
            // it will randomly pick an attack from the 3 options using int vars (3 is not included when its not float meaning its not 3f)-
            enemyAnim.EnemyAttack(Random.Range(0, 3));
            // reset the values so he will wait againg before attacking again-
            current_Attack_Time = 0f;
        }
        // check if after attacking you are not in the attack distance anymore-
        if (Vector3.Distance(transform.position, playerTarget.position) > attack_Distance + chase_Player_After_Attack)
        {
            // then enemy will stop attack and start follow-
            attackPlayer = false;
            followPlayer = true;
        }
    }

}
