﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// creating enum of the Makot list-
public enum Combostate
{
    NONE,
    PUNCH_1,
    PUNCH_2,
    PUNCH_3,
    KICK_1,
    KICK_2
}


public class PlayerAttack : MonoBehaviour
{
    //access to Anmimation of the player
    private CharacterAnimation player_Anim;

    // creating bool for Combo Cooldown-
    private bool activeteTimerToReset;

    // creating var for default combo timer (you have 0.4f to press key again to continue combo or it ends and start over) + current combo timer to use when needed -
    private float default_Combo_Timer = 0.4f;
    private float current_Combo_Timer;

    // creating var for the combostate that was created as an enum above-
    private Combostate current_Combo_State;

    private bool animation_Lock;

    private void Start()
    {
        //no combo is activated so the state is none and has no timer-
        current_Combo_Timer = default_Combo_Timer;
        current_Combo_State = Combostate.NONE;
    }

    void Awake()
    {
        player_Anim = GetComponentInChildren<CharacterAnimation>();
        animation_Lock = false;
    }

    void Update()
    {
        // check if Makot is hapanning-
        ComboAttacks();
        ResetComboState();
    }
    void ComboAttacks()
    {
        if (Input.GetKeyDown(KeyCode.Z) && animation_Lock == false)
        {
            // we need to check that we are not in the end of the combo which is Punch3 or Kick 1 & 2. if so then combo is finished-
            if (current_Combo_State == Combostate.PUNCH_3 || current_Combo_State == Combostate.KICK_1 || current_Combo_State == Combostate.KICK_2)
                return;
            //combo starts, so timer starts and combostate list is starting adding ++ which any key press-
            current_Combo_State++;
            activeteTimerToReset = true;
            current_Combo_Timer = default_Combo_Timer;

            // combo sequence starts here-
            if (current_Combo_State == Combostate.PUNCH_1)
            {
                player_Anim.Punch_1();
            }

            if (current_Combo_State == Combostate.PUNCH_2)
            {
                player_Anim.Punch_2();
            }
            if (current_Combo_State == Combostate.PUNCH_3)
            {
                player_Anim.Punch_3();
            }
        }

        if (Input.GetKeyDown(KeyCode.X) && animation_Lock == false)
        {
            // we need to check that we are not in the end of the combo which is Punch3 or Kick 2, if so then combo is finished-
            if (current_Combo_State == Combostate.PUNCH_3 || current_Combo_State == Combostate.KICK_2)
                return;
            // checking if we are not on the end of the combo, meaning punch1,punch2,none -
            if (current_Combo_State == Combostate.NONE || current_Combo_State == Combostate.PUNCH_1 || current_Combo_State == Combostate.PUNCH_2)
            // so the combo proceed and next move in the combo is kick 1-
            {
                current_Combo_State = Combostate.KICK_1;
            }
            else if (current_Combo_State == Combostate.KICK_1)
            {
                current_Combo_State++;
            }
            //activation of the combo timer is on-
            activeteTimerToReset = true;
            current_Combo_Timer = default_Combo_Timer;

            //activation of animation of the kicks when is pressed-
            if (current_Combo_State == Combostate.KICK_1)
            {
                player_Anim.Kick_1();
            }
            if (current_Combo_State == Combostate.KICK_2)
            {
                player_Anim.Kick_2();
            }
        }
        // if were aleready in the kick then moving in the chain to kick 2-
        
    }
    void ResetComboState()
    {
        // if combo starts then timer to reset bool is true and starts. when ends, combo also ends and combo sequens starts from begginig if activated again-
        if (activeteTimerToReset)
        {
            current_Combo_Timer -= Time.deltaTime;
            //if there's no time left then combo ends-
            if(current_Combo_Timer <= 0f)
            {
                current_Combo_State = Combostate.NONE;
                activeteTimerToReset = false;
                current_Combo_Timer = default_Combo_Timer;
            }
        }
    } // reset Combo state

    public void LockAnimation()
    {
        animation_Lock = true;
    }
    
    public void UnLockAnimation()
    {
        animation_Lock = false;
    }
}
