﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementTutorial : MonoBehaviour
{
    //Rigidbody's Var-
    private Rigidbody myBody;

    //Acces to Player's Animator-
    private CharacterAnimation player_Anim;

    //Speed for player-
    public float walk_Speed = 2f;
    public float z_speed = 1.5f;

    // Player's right Rotation angle and speed-
    private float rotation_Y = -90;
    private float rotation_Speed = 15f;

    private bool movement_Enabled;

    void Awake()
    {
        //Access to Rigidbody's & Animator using var-
        myBody = GetComponent<Rigidbody>();
        player_Anim = GetComponentInChildren<CharacterAnimation>();
        movement_Enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        // changing Player's transform will happen in Update.
        // every frame a check for rotation change or animation for walking-
        RotatePlayer();
        AnimatePlayerWalk();
    }

    private void FixedUpdate()
    {
        // Any physics Changed Is supposed to be on FixedUpdated- 
        if (movement_Enabled)
        {
            DetectMovement();
        }
    }
    //Function of the Controller Etablishement using Vars from TagManager Script * Negetive speed is Moving the velocity (Speed*time) of the rigidbody using var-
    void DetectMovement()
    {
        myBody.velocity = new Vector3(Input.GetAxisRaw(Axis.HORIZONTAL_AXIS) * (-walk_Speed), myBody.velocity.y, Input.GetAxisRaw(Axis.VERTICAL_AXIS) * (-z_speed));
    }

    //Function for the Rotation of the player affected by movement. roatating to direction of movement-
    void RotatePlayer()
    {
        //if player goes right side then its value is 1-
        if (Input.GetAxisRaw(Axis.HORIZONTAL_AXIS) > 0)
        {
            // rotation to y=-90
            transform.rotation = Quaternion.Euler(0f, rotation_Y, 0f);
        }
        //if player goes Left side then its value is -1-
        else if (Input.GetAxisRaw(Axis.HORIZONTAL_AXIS) < 0)
        {
            // quaternion represent rotation, Mathf.Abs is to make the inside number absolute meaning positive value. rotation to y=90-
            transform.rotation = Quaternion.Euler(0f, Mathf.Abs(rotation_Y), 0f);
        } 
    }

    void AnimatePlayerWalk()
    {

        //Check if Key is pressed (check that value isn't 0) in order to declare Movement and start animation-
        var isMovementPressed = Input.GetAxisRaw(Axis.HORIZONTAL_AXIS) != 0 || (Input.GetAxisRaw(Axis.VERTICAL_AXIS) != 0);
        player_Anim.Walk(isMovementPressed);
        //if ()
        //{
        //    //Bool of movement is set to true
            
        //}
        //else
        //{
        //    player_Anim.Walk(false);
        //}
    }

    public void DisablePlayerMovement()
    {
        movement_Enabled = false;
    }

    public void EnablePlayerMovement()
    {
        movement_Enabled = true;
    }
}

