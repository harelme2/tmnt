﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeactivateGameObject : MonoBehaviour
{
    public float timer = 2f;
    void Start()
    {
        // Start invoke and stop after 2f-
        Invoke("DeactivateAfterTime", timer);
    }

   void DeactivateAfterTime()
    {
        // Set off-
        gameObject.SetActive(false);
    }
}
