﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthUI : MonoBehaviour
{
    // access to image with var-
    private Image health_UI;
    void Awake()
    {
        // acess to to image using Tag script-
        health_UI = GameObject.FindWithTag(Tags.HEALTH_UI).GetComponent<Image>();
    }
    void Update()
    {
        
    }

    // A function to contorl the health bar-
    public void DisplayHealth(float value)
    {
        // we devid to 100 so the health bar is in relitivety with 100%- 
        value /= 100f;
        // if the value is less then 0 it will remain 0-
        if (value < 0f)
            value = 0f;
        //acess to the fill amount it the picture and make it relative to 100-
        health_UI.fillAmount = value;
    }
}
