﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackUniversal : MonoBehaviour
{
    // crating an inspector panel choose which layer to affect-
    public LayerMask collisionLayer;
    public float radius = 1f;
    public float damage = 2f;

    public bool is_Player, is_Enemy;

    public GameObject hit_FX_Prefab;
   
    void Update()
    {
        DetectCollision();
    }

    void DetectCollision()
    {
        // creating array that is checking if an unvisible spehre is hitting a colision layer, we want to detect collision only when its beacause of an attack-
        Collider[] hit = Physics.OverlapSphere(transform.position, radius, collisionLayer);
        // if theres a hit then its bigger then 0 -
        if (hit.Length == 0)
        {
            return;
        }
        //check that the player is the one who is attacking-
        if (is_Player)
        {
            HandlePlayerHit(hit);
        }

        // script for if its the enemy-
        if (is_Enemy)
        {
            // apply damage but no knockdown-
            hit[0].GetComponent<HealthScript>().ApplyDamage(damage, false);
        }
        //after the hit we want to deactive the collider-
        gameObject.SetActive(false);
    }

    private void HandlePlayerHit(Collider[] hit)
    {
        //Getting the position of the enemy to spawn FX where he is hit-
        Vector3 hitFX_Pos = hit[0].transform.position;
        // adding to Y position some legnth after test to be excatcly in the head of the enemy-
        hitFX_Pos.y += 1.3f;

        //if the enemy is facing the right side-
        if (hit[0].transform.forward.x > 0)
        {
            // Adding some length to x position after test to be exactly in the head of the enemy-
            hitFX_Pos.x += 0.3f;
        }
        // if the enemy is facing the left side-
        else if (hit[0].transform.forward.x < 0)
        {
            // Adding some length to x position after test to be exactly in the head of the enemy-
            hitFX_Pos.x -= 0.3f;
        }
        //Spawn hit effect from the direction its supposed to come from-
        Instantiate(hit_FX_Prefab, hitFX_Pos, Quaternion.identity);
        //check if there's a hit in order to create damage-
        // is there a collision with tags of punches & Kicks of the last punch and kick of the combo-
        if (gameObject.CompareTag(Tags.LEFT_ARM_TAG) || gameObject.CompareTag(Tags.LEFT_LEG_TAG) || gameObject.CompareTag(Tags.RIGHT_ARM_TAG) || gameObject.CompareTag(Tags.RIGHT_LEG_TAG) || gameObject.CompareTag(Tags.STICK_TAG))
        {
            // damage is done and is made through another script, knockdown is true-
            hit[0].GetComponent<HealthScript>().ApplyDamage(damage, true);
        }
        // if its not the ending punch/kick and it makes damage but won't creat KnockDown-
        else
        {
            hit[0].GetComponent<HealthScript>().ApplyDamage(damage, false);
        }
    }
}
